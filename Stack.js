// Constructor to initialize the stack
class Stack {
    constructor() {
        this.stack = []
    }


    // Pushes a new element on top of the stack
    pushe(element) {
        this.stack[this.stack.length] = element
        return this.stack
    }

    // Removes the top most element from the stack and returns that element
    pope() {
        let m = this.stack[this.stack.length - 1]
        this.stack.length = this.stack.length - 1
        return m
    }

    // Returns the top-most element, but doesn't change the stack
    tope() {
        return this.stack[this.stack.length - 1]
    }

    // Returns true if stack has no elements in it, otherwise false
    isEmptye() {
        return this.stack[this.stack.length - 1] ? false : true;
    }

    // Removes all elements from the stack
    cleare() {
        this.stack.length = this.stack.length - 1
        if (this.stack[this.stack.length - 1]) { this.cleare() } else { return true }

    }
}
let a = new Stack

a.pushe(3)
a.pushe(8)

console.log(a.tope())
console.log(a.pope())
console.log(a.stack.length)
console.log(a.cleare())

console.log(a.isEmptye())